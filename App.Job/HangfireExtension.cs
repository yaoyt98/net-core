﻿using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System.ComponentModel;

namespace App.Job
{
    public static class HangfireExtension
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="sqlDns"></param>
        public static void UseHangfireExtension(this IServiceCollection services, string sqlDns)
        {
            services.AddHangfire(configuration => configuration
               .SetDataCompatibilityLevel(CompatibilityLevel.Version_180)
               .UseSimpleAssemblyNameTypeSerializer()
               .UseRecommendedSerializerSettings()
               .UseSqlServerStorage(sqlDns));

            // 自动重试机制， 重试3次
            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute { Attempts = 3 });

            // Add the processing server as IHostedService
            services.AddHangfireServer();
        }
    

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="sqlDns"></param>
        public static void UseHangfire(this WebApplication app)
        {
            // 开启仪表盘
            app.UseHangfireDashboard();
        }
    }

}
