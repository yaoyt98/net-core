﻿namespace App.Casbin
{
    public class AppRuleDto
    {
        /// <summary>
        /// 权限中文
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 权限名称英文
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// 权限动作
        /// </summary>
        public List<AppActionRuleDto> actionRuleItems { get; set; } = new List<AppActionRuleDto>();

    }

    public class AppActionRuleDto
    {
        /// <summary>
        /// 权限中文
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 权限名称英文
        /// </summary>
        public string value { get; set; }
    }
}
