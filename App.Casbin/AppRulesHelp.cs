﻿using System.Reflection;

namespace App.Casbin
{
    public static class AppRulesHelp
    {

        /// <summary>
        /// 所有权限菜单类
        /// </summary>
        public static readonly List<AppRuleDto> RulesItems = new List<AppRuleDto>();

        /// <summary>
        /// 
        /// </summary>
        static AppRulesHelp()
        {
            var currAssem = AppDomain.CurrentDomain.GetAssemblies();

            var assemItems = currAssem.Where(p => p.GetTypes().Any(w => w.FullName.EndsWith("Controller"))).ToList();

            foreach (var AssemItem in assemItems)
            {
                var controllerItems = AssemItem.GetTypes().Where(p => p.GetCustomAttribute<AppRuleAttribute>() != null);
                foreach (var controller in controllerItems)
                {
                    var appRuleAtt = controller.GetCustomAttribute<AppRuleAttribute>();
                    if (appRuleAtt == null)
                    {
                        continue;
                    }

                    var rule = new AppRuleDto();
                    rule = appRuleAtt.AppRuleItem;

                    var appRuleActionAtts = controller.GetCustomAttributes<AppActionRuleAttribute>().ToList();
                    if (appRuleActionAtts == null || appRuleActionAtts.Count == 0)
                    {
                        continue;
                    }

                    foreach (var appRuleActionAtt in appRuleActionAtts)
                    {
                        rule.actionRuleItems.Add(appRuleActionAtt.AppActRuleItems);
                    }

                    RulesItems.Add(rule);
                }
            }

        }
    }
}
