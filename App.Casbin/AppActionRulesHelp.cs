﻿namespace App.Casbin
{
    public static class AppActionRulesHelp
    {
        /// <summary>
        /// 查询
        /// </summary>
        public const string get = "查询";

        /// <summary>
        /// 修改
        /// </summary>
        public const string update = "修改";

        /// <summary>
        /// 新增
        /// </summary>
        public const string add = "新增";

        /// <summary>
        /// 新增和修改
        /// </summary>
        public const string addAndUpdate = "新增和修改";

        /// <summary>
        /// 删除
        /// </summary>
        public const string delete = "删除";

        /// <summary>
        /// 审核
        /// </summary>
        public const string audit = "审核";


        /// <summary>
        /// 上传
        /// </summary>
        public const string uploading = "上传";


        /// <summary>
        /// 下载
        /// </summary>
        public const string download = "下载";


        /// <summary>
        /// 启用
        /// </summary>
        public const string on = "启用";

        /// <summary>
        /// 关闭
        /// </summary>
        public const string off = "关闭";

        /// <summary>
        /// 授权
        /// </summary>
        public const string author = "授权";
    }
}
