﻿using Casbin;
using Casbin.AspNetCore.Authorization;
using Casbin.AspNetCore.Authorization.Transformers;
using Casbin.Persist.Adapter.EFCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;

namespace App.Casbin
{
    /// <summary>
    /// casbin 权限服务
    /// 类,api使用特性 [CasbinAuthorize("data2", "write")]
    /// </summary>
    public static class CasbinExtion
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="casbinConfigName">文件夹名称</param>
        /// <param name="modelName">model.conf 全称+ 后缀</param>
        /// <param name="policyName">policy.csv 全称+ 后缀</param>
        public static void UseCasbinExtion(this IServiceCollection services, string casbinConfigName, string modelName, string policyName, string sqlDb)
        {
            services.AddDbContext<CasbinDbContext<int>>(options => options.UseSqlServer(sqlDb));

            services.AddCasbinAuthorization(options =>
            {
                options.PreferSubClaimType = ClaimTypes.Name;
                options.DefaultModelPath = Path.Combine(casbinConfigName, modelName);
                //options.DefaultPolicyPath = Path.Combine(casbinConfigName, policyName);

                // Comment line below to use the default BasicRequestTransformer
                // Note: Commenting the line means that the action methods MUST have [CasbinAuthorize()] attribute which explicitly specifies obj and policy. Otherwise authorization will be denied
                // 这里我们采用基础权限方式
                options.DefaultRequestTransformerType = typeof(BasicRequestTransformer);

                // 这里我们引入efcore 处理数据
                options.DefaultEnforcerFactory = (p, m) => new Enforcer(m,
                    new EFCoreAdapter<int>(p.GetRequiredService<CasbinDbContext<int>>())
                    );
            });
        }

        public static void UseCasbinExtion(this IApplicationBuilder app)
        {
            app.UseCasbinAuthorization();
        }
    }
}
