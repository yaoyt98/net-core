﻿namespace App.Casbin
{

    /// <summary>
    /// 权限名称
    /// 
    /// [AppRule("登录模块", nameof(AugUserController))]
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class AppRuleAttribute : Attribute
    {
        public AppRuleDto AppRuleItem = new AppRuleDto();

        public AppRuleAttribute()
        {
        }

        public AppRuleAttribute(string ruleModelName, string ruleModelValue)
        {
            AppRuleItem.name = ruleModelName;
            AppRuleItem.value = ruleModelValue;
        }
    }

    /// <summary>
    /// 动作权限名称
    /// [AppActionRule("登录", nameof(Login))]
    /// [AppActionRule("编辑", "update")]
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class AppActionRuleAttribute : Attribute
    {
        public AppActionRuleDto AppActRuleItems = new AppActionRuleDto();

        public AppActionRuleAttribute(string ruleModelName, string ruleModelValue)
        {
            AppActRuleItems.name = ruleModelName;
            AppActRuleItems.value = ruleModelValue;
        }
    }
}
