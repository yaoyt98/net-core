﻿using Microsoft.AspNetCore.Builder;

namespace App.Tool
{
    /// <summary>
    /// 手动di扩展
    /// </summary>
    public static class ServiceLocatorExtension
    {
        public static IApplicationBuilder UseServiceLocator(this IApplicationBuilder app)
        {
            ServiceLocator.Instance = app.ApplicationServices;

            return app;
        }

    }
}
