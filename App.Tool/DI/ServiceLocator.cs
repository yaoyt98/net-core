﻿namespace App.Tool
{
    /// <summary>
    /// 获取注入服务
    /// </summary>
    public static class ServiceLocator
    {
        /// <summary>
        /// 获取服务
        /// </summary>
        public static IServiceProvider? Instance { get; set; }
    }
}
