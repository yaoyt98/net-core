﻿using System.ComponentModel;

namespace App.Tool
{
    /// <summary>
    /// 枚举
    /// </summary>
    public static class EnumHelp
    {

        /// <summary>
        /// 获取枚举注释
        /// DescriptionAttribute
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum val)
        {
            var field = val.GetType().GetField(val.ToString());
            var attri = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute));
            var result = attri == null ? val.ToString() : ((DescriptionAttribute)attri).Description;
            return result;
        }

    }
}
