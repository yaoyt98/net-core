﻿using System.Security.Cryptography;
using System.Text;

namespace App.Tool
{
    public static class sha1Help
    {

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="str"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static string Sha1Encryp(string str, Encoding? encoding = null)
        {
            if (encoding == null) encoding = Encoding.UTF8;
            var buffer = encoding.GetBytes(str);
            var data = SHA1.Create().ComputeHash(buffer);

            var sub = new StringBuilder();
            foreach (var t in data)
            {
                sub.Append(t.ToString("x2"));
            }

            return sub.ToString();
        }
    }
}
