﻿using System.Security.Cryptography;
using System.Text;

namespace App.Tool
{
    public static class md5Help
    {
        /// <summary>
        /// md5 32位加密
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string Md5Encryp32(string password)
        {
            string pwd = "";
            MD5 md5 = MD5.Create();
            var s = md5.ComputeHash(Encoding.UTF8.GetBytes(password));
            for (int i = 0; i < s.Length; i++)
            {
                pwd = pwd + s[i].ToString("x2");
            }
            return pwd;
        }
    }
}