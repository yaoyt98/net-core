﻿namespace App.Tool
{

    /// <summary>
    /// 时间帮助类
    /// </summary>
    public static class DateTimeHelp
    {
        public static int GetDateTimeType(this DateTime dt, AppEnumDateTimeType type)
        {
            int tempNum;

            switch (type)
            {
                case AppEnumDateTimeType.day:
                    tempNum = dt.Day;
                    break;
                case AppEnumDateTimeType.week:

                    //得到当月第一天
                    var firstDay = dt.AddDays(1 - dt.Day);
                    // 第一天是周几
                    var firestWeekday = firstDay.DayOfWeek == 0 ? 7 : (int)firstDay.DayOfWeek;
                    //本月第一周有几天
                    var firstWeekEndDay = 7 - (firestWeekday - 1);

                    //当前日期和第一周之差
                    var diffday = dt.Day - firstWeekEndDay;
                    diffday = diffday > 0 ? diffday : 1;

                    //当前是第几周,如果整除7就减一天
                    var tempWeek = (diffday % 7) == 0 ? (diffday / 7 - 1) : (diffday / 7);
                    //周
                    tempNum = tempWeek + 1 + (dt.Day > firstWeekEndDay ? 1 : 0);

                    break;
                case AppEnumDateTimeType.month:
                    tempNum = dt.Month;
                    break;
                case AppEnumDateTimeType.season:
                    tempNum = ((dt.Month) + 3 - 1) / 3;
                    break;
                case AppEnumDateTimeType.year:
                    tempNum = dt.Year;
                    break;
                default:
                    tempNum = dt.Day;
                    break;
            }

            return tempNum;
        }


        public static int Week(this DateTime dt)
        {

            return dt.GetDateTimeType(AppEnumDateTimeType.week);
        }

        public static int Season(this DateTime dt)
        {

            return dt.GetDateTimeType(AppEnumDateTimeType.season);
        }
    }

}
