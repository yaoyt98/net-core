﻿using System.ComponentModel;

namespace App.Tool
{
    public enum AppEnumDateTimeType : uint
    {
        /// <summary>
        /// 天
        /// </summary>
        [Description("天")]
        day,

        /// <summary>
        /// 周
        /// </summary>
        [Description("周")]
        week,

        /// <summary>
        /// 月
        /// </summary>
        [Description("月")]
        month,

        /// <summary>
        /// 季度
        /// </summary>
        [Description("季度")]
        season,

        /// <summary>
        /// 年
        /// </summary>
        [Description("年")]
        year,
    }
}
