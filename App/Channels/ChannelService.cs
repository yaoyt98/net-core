﻿using Microsoft.Extensions.Hosting;

namespace App.Channels
{
    public class ChannelService : BackgroundService
    {

        private readonly IChannelMsg<int> _channel;
        public ChannelService(IChannelMsg<int> channel)
        {
            _channel = channel;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await _channel.ReadAsync(p =>
                {

                    // 处理消息内容

                });
            }
        }
    }
}
