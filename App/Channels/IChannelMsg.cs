﻿namespace App.Channels
{
    public interface IChannelMsg<T>
    {
        Task WriteAsync(T reqDto, bool isNoWrite = true);
        Task ReadAsync(Action<T> func);
    }
}
