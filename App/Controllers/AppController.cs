﻿using App.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace App
{

    [Authorize]
    [ApiController]
    [ApiVersionCustom(AppApiVersion.V1)]
    public class AppController : ControllerBase
    {
        public int? userId
        {
            get
            {
                if (User == null)
                {
                    return null;
                }

                var user = User.Claims.FirstOrDefault(p => p.Type == "userId")?.Value;

                return string.IsNullOrWhiteSpace(user) ? null : Convert.ToInt32(user);
            }
        }

        /// <summary>
        /// 是否是超级管理员
        /// </summary>
        public bool? isAdmin
        {
            get
            {
                if (User == null)
                {
                    return null;
                }

                var user = User.Claims.FirstOrDefault(p => p.Type == "isAdmin");

                return user != null;
            }
        }

        /// <summary>
        /// mapper 自动注入
        /// </summary>
        public readonly IMapper _mapper;
        public AppController()
        {
        }


        public AppController(IMapper mapper)
        {
            _mapper = mapper;
        }

        /// <summary>
        /// 成功状态返回结果
        /// </summary>
        /// <param name="result">返回的数据</param>
        /// <returns></returns>
        public static AppResponse<T> SuccessResult<T>(T data)
        {
            return new AppResponse<T>(data);
        }

        /// <summary>
        /// 失败状态返回结果
        /// </summary>
        /// <param name="code">状态码</param>
        /// <param name="msg">失败信息</param>
        /// <returns></returns>
        public static AppResponse<T> FailResult<T>(string? msg = null)
        {
            return new AppResponse<T>(msg);
        }

        /// <summary>
        /// 错误返回结果
        /// </summary>
        /// <param name="status"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static AppResponse<T> ErrorResult<T>(Exception err)
        {
            return new AppResponse<T>(err);
        }
    }
}
