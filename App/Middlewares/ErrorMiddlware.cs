﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace App
{
    /// <summary>
    /// 异常组件
    /// </summary>
    public class ErrorMiddlware
    {

        private readonly RequestDelegate _next;
        private ILogger<ErrorMiddlware> _logger;
        public ErrorMiddlware(RequestDelegate next, ILogger<ErrorMiddlware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var response = context.Response;
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                try
                {
                    response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                    await response.WriteAsJsonAsync(ex.Message);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "出现异常");
                }
            }
        }
    }

    public static class ErrorMiddlwareExtensions
    {
        public static IApplicationBuilder UseError(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ErrorMiddlware>();
        }
    }
}
