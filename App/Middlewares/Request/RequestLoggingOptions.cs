﻿//using Microsoft.AspNetCore.Http;
//using Microsoft.Extensions.Logging;

//namespace App.Middlewares.Request {

//    /// <summary>
//    /// 请求类， 设置api日志参数设置
//    /// </summary>
//    public class RequestLoggingOptions {

//        /// <summary>
//        /// 得到日志
//        /// </summary>
//        public Func<HttpContext, LogLevel> GetLevel { get; set; }

//        public Action<HttpContext, IDiagnosticContext>? EnrichDiagnosticContext { get; set; }

//        public Microsoft.Extensions.Logging.ILogger? Logger { get; set; }

//        /// <summary>
//        /// 得到默认日志登记
//        /// </summary>
//        /// <param name="ctx"></param>
//        /// <param name="_"></param>
//        /// <param name="ex"></param>
//        /// <returns></returns>
//        private static LogLevel DefaultGetLevel(HttpContext ctx, double _, Exception? ex)
//        {
//            if (ex == null)
//            {
//                if (ctx.Response.StatusCode <= 499)
//                {
//                    return LogLevel.Information;
//                }

//                return LogLevel.Error;
//            }

//            return LogLevel.Error;
//        }

//    }

//    public interface IDiagnosticContext {
//        void Set(string name, object value);

//    }
//}
