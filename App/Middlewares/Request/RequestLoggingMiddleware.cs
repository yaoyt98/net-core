﻿//using Microsoft.AspNetCore.Builder;
//using Microsoft.AspNetCore.Http;
//using Microsoft.Extensions.Logging;
//using System.IO;

//namespace App.Middlewares.Request {
//    public class RequestLoggingMiddleware {

//        private readonly RequestDelegate _next;
//        private ILogger<RequestLoggingMiddleware> _logger;
//        public RequestLoggingMiddleware(RequestDelegate next, ILogger<RequestLoggingMiddleware> logger)
//        {
//            _next = next;
//            _logger = logger;
//        }

//        public async Task InvokeAsync(HttpContext context, RequestLoggingOptions reqLogOptions)
//        {
//            var request = context.Request;
//            var response = context.Response;

//            var path = request.Path.Value;
//            var protocol = request.Protocol;
//            var queryStr = request.QueryString.Value ?? "";
//            var contentType = request.ContentType ?? "";

//            var bodySteam = request.Body;
//            var bodyBytes = new byte[request.Body.Length];
//            bodySteam.Read(bodyBytes, 0, bodyBytes.Length);
//            var bodyStr = Encoding.UTF8.GetString(bodyBytes);

//            try
//            {
//                await _next(context);
//            }
//            catch (Exception ex)
//            {
//                try
//                {
//                    var result = new AppResponse<string>(ex);
//                    await response.WriteAsJsonAsync(result);
//                }
//                catch (Exception e)
//                {
//                    _logger.LogError(e, "出现异常");
//                }
//            }
//        }
//    }

//    public static class RequestLoggingMiddlwareExtensions {
//        public static IApplicationBuilder UseRequestLogging(this IApplicationBuilder builder, Action<RequestLoggingOptions>? reqLogOptions)
//        {
//            var option = reqLogOptions?.Invoke();


//            return builder.UseMiddleware<RequestLoggingMiddleware>(option);
//        }
//    }
//}
