﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace App.Json
{
    //时间格式化响应
    public class JsonDateTimeConverter1 : JsonConverter<DateTime>
    {
        private readonly string Format;
        public JsonDateTimeConverter1(string format)
        {
            Format = format;
        }
        public override void Write(Utf8JsonWriter writer, DateTime date, JsonSerializerOptions options)
        {
            writer.WriteStringValue(date.ToString(Format));
        }
        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return DateTime.ParseExact(reader.GetString(), Format, null);
        }

    }
}
