﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;

namespace App.Attributes
{
    /// <summary>
    /// 自定义属性路由
    /// 功能
    /// 1. 版本控制
    /// 2. api是否对外控制
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]

    public class ApiVersionCustomAttribute : RouteAttribute, IApiDescriptionGroupNameProvider, IApiDescriptionVisibilityProvider
    {
        private readonly static string _template = "/api/{version}/[controller]/[action]";

        /// <summary>
        /// 分组名称,是来实现接口 IApiDescriptionGroupNameProvider
        /// </summary>
        public string GroupName { get; set; } = AppApiVersion.V1;

        /// <summary>
        /// api忽略 IApiDescriptionVisibilityProvider
        /// </summary>
        public bool IgnoreApi { get; set; } = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionName"></param>
        public ApiVersionCustomAttribute() : base(_template)
        {
        }

        /// <summary>
        /// 自定义版本+路由构造函数
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="version"></param>
        public ApiVersionCustomAttribute(string groupName) : base($"/api/{groupName}/[controller]/[action]")
        {
            GroupName = groupName;
        }

        /// <summary>
        /// 路由构造函数
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="version"></param>
        public ApiVersionCustomAttribute(bool ignoreApi) : base(_template)
        {
            IgnoreApi = ignoreApi;
        }

        /// <summary>
        /// 自定义版本+路由构造函数
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="version"></param>
        public ApiVersionCustomAttribute(string groupName, bool ignoreApi) : base($"/api/{groupName}/[controller]/[action]")
        {
            GroupName = groupName;
            IgnoreApi = ignoreApi;
        }

    }
}
