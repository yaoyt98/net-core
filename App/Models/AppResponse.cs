﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace App.Models
{
    /// <summary>
    /// api返回类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DataContract]
    public class AppResponse<T>
    {

        [DataMember]
        public AppResponseStatus Status { get; set; } = AppResponseStatus.Success;
        [DataMember]
        public string Msg { get; set; } = String.Empty;
        [DataMember]
        public T? Data { get; set; } = default;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        public AppResponse(string msg)
        {
            Status = AppResponseStatus.Success;
            Msg = msg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        public AppResponse(T val)
        {
            Data = val;
            Status = AppResponseStatus.Success;
            Msg = "成功";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Status"></param>
        /// <param name="val"></param>
        public AppResponse(AppResponseStatus status, T? val = default)
        {
            Status = Status;
            Data = val;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Status"></param>
        /// <param name="val"></param>
        public AppResponse(AppResponseStatus status, string msg, T? val = default)
        {
            Status = status;
            Msg = msg;
            Data = val;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Status"></param>
        /// <param name="val"></param>
        public AppResponse(Exception err)
        {
            Status = AppResponseStatus.Error;
            Msg = err.Message;
            Data = default;
        }


        #region 这里我们定义几个 方法

        /// <summary>
        /// 成功状态返回结果
        /// </summary>
        /// <param name="result">返回的数据</param>
        /// <returns></returns>
        public static AppResponse<T> SuccessResult(T data)
        {
            return new AppResponse<T>(data);
        }

        /// <summary>
        /// 失败状态返回结果
        /// </summary>
        /// <param name="code">状态码</param>
        /// <param name="msg">失败信息</param>
        /// <returns></returns>
        public static AppResponse<T> FailResult(string msg)
        {
            return new AppResponse<T>(AppResponseStatus.Fail, msg);
        }

        /// <summary>
        /// 错误返回结果
        /// </summary>
        /// <param name="status"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static AppResponse<T> ErrorResult(Exception err)
        {
            return new AppResponse<T>(err);
        }


        #endregion

        /// <summary>
        /// 隐式将T转化为AppResponse<T>
        /// </summary>
        /// <param name="value"></param>
        public static implicit operator AppResponse<T>(T value)
        {
            return new AppResponse<T>(value);
        }
    }

    public enum AppResponseStatus
    {
        [Description("成功")]
        Success = 200,
        [Description("失败")]
        Fail = 0,
        [Description("错误")]
        Error = -1,
    }
}
