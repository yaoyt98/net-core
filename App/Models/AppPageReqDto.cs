﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace App.Models
{

    /// <summary>
    /// 分页请求类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AppPageReqDto<T>
    {
        [Required(ErrorMessage = "当页参数必填")]
        [Range(1, 9999, ErrorMessage = "当页参数必须大于0")]
        public int pageIndex { get; set; } = 1;

        [Required(ErrorMessage = "页码参数必填")]
        [Range(1, 9999, ErrorMessage = "页码参数必须大于0")]
        public int pageSize { get; set; } = 30;

        /// <summary>
        /// 跳过多少页
        /// </summary>
        [JsonIgnore]
        public int skipNum
        {
            get
            {
                return (pageIndex - 1) * pageSize;
            }
        }

        public T? data { get; set; } = default;

    }
}
