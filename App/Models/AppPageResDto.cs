﻿namespace App.Models
{
    /// <summary>
    /// 分页出参类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AppPageResDto<T>
    {

        public AppPageResDto(int pageIndex, int pageSize)
        {
            this.pageIndex = pageIndex;
            this.pageSize = pageSize;
            this.totalNum = 0;
            this.data = default;
        }

        public AppPageResDto(int pageIndex, int pageSize, int totalNum, List<T> data)
        {
            this.pageIndex = pageIndex;
            this.pageSize = pageSize;
            this.totalNum = totalNum;
            this.data = data;
        }

        public int pageIndex { get; set; } = 0;

        public int pageSize { get; set; } = 30;

        /// <summary>
        /// 数据总条数
        /// </summary>
        public int totalNum { get; set; } = 0;

        /// <summary>
        /// 总页数
        /// </summary>
        public int totalPage
        {
            get
            {
                return ((totalNum - 1) / pageSize) + 1;
            }
        }

        /// <summary>
        /// 返回数据
        /// </summary>
        public List<T>? data { get; set; } = default(List<T>);
    }
}