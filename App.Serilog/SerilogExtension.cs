﻿using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.MSSqlServer;
using System.Collections.ObjectModel;
using System.Data;


namespace App.Serilog
{
    public static class SerilogExtension
    {

        /// <summary>
        /// 开启日志
        /// </summary>
        /// <param name="services"></param>
        /// <param name="sqlDns"></param>
        public static void UseSerilogExtension(this IServiceCollection services, string sqlDns, string tableName)
        {
            // 自定义字段
            var columnOptions = new ColumnOptions
            {
                AdditionalColumns = new Collection<SqlColumn> {
                    new SqlColumn  { DataType=SqlDbType.VarChar, ColumnName="user"},
                    new SqlColumn  { DataType=SqlDbType.VarChar, ColumnName="groupId"}
                }
            };

            Log.Logger = new LoggerConfiguration()
                  .Enrich.FromLogContext()
                  .Enrich.WithProperty("Version", "1.0.0")
                   ////日志打印到文件上
                   //.WriteTo.Logger(p => p.Filter.ByIncludingOnly(w => w.Level == LogEventLevel.Information)
                   //        .WriteTo.Async(w => w.File(LogFilePath("Info"), rollingInterval: RollingInterval.Day, rollOnFileSizeLimit: true, restrictedToMinimumLevel: LogEventLevel.Information))
                   //        )
                   //.WriteTo.Logger(p => p.Filter.ByIncludingOnly(w => w.Level == LogEventLevel.Warning)
                   //        .WriteTo.Async(w => w.File(LogFilePath("Warning"), rollingInterval: RollingInterval.Day, rollOnFileSizeLimit: true, restrictedToMinimumLevel: LogEventLevel.Warning))
                   //        )
                   //.WriteTo.Logger(p => p.Filter.ByIncludingOnly(w => w.Level == LogEventLevel.Error)
                   //        .WriteTo.Async(w => w.File(LogFilePath("Error"), rollingInterval: RollingInterval.Day, rollOnFileSizeLimit: true, restrictedToMinimumLevel: LogEventLevel.Error))
                   //        )
                   // .WriteTo.Logger(p => p.Filter.ByIncludingOnly(w => w.Level == LogEventLevel.Fatal)
                   //        .WriteTo.Async(w => w.File(LogFilePath("Fatal"), rollingInterval: RollingInterval.Day, rollOnFileSizeLimit: true, restrictedToMinimumLevel: LogEventLevel.Fatal))
                   //        )

                   .WriteTo.Logger(p => p.Filter.ByIncludingOnly(w => w.Level == LogEventLevel.Fatal || w.Level == LogEventLevel.Error)
                       .WriteTo.Async(a => a.MSSqlServer(sqlDns, new MSSqlServerSinkOptions
                       {
                           TableName = tableName,
                           AutoCreateSqlTable = true,
                       }, columnOptions: columnOptions))
                   )
#if DEBUG
                  .MinimumLevel.Debug()//最小日志等级
                  .WriteTo.Console() //日志打印到控制台  
#else
                  .MinimumLevel.Error()//最小日志等级
#endif
                 .CreateBootstrapLogger();

        }

    }
}
