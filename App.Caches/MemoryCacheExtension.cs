﻿using Microsoft.Extensions.DependencyInjection;

namespace App.Caches
{
    public static class MemoryCacheExtension
    {
        /// <summary>
        /// 开启自定义缓存
        /// </summary>
        /// <param name="services"></param>
        public static void UseAppCacheExtension(this IServiceCollection services)
        {
            services.AddSingleton<AppMemoryCache>();
        }

        /// <summary>
        /// 开启默认缓存
        /// </summary>
        /// <param name="services"></param>
        public static void UseDefaultCacheExtension(this IServiceCollection services)
        {
            services.AddMemoryCache();
        }
    }
}
