﻿using Microsoft.Extensions.Caching.Memory;

namespace App.Caches
{
    /// <summary>
    /// 内存缓存
    /// </summary>
    public class AppMemoryCache
    {
        public MemoryCache Cache { get; } = new MemoryCache(
            new MemoryCacheOptions
            {
                // 设置缓存大小
                SizeLimit = 1024
            });
    }
}
