﻿namespace App.SqlSugar
{
    public interface IEntityUpdate<T, T1>
    {
        T updateId { get; set; }
        T1 updateDt { get; set; }
    }
}
