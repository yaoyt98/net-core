﻿namespace App.SqlSugar
{
    public interface IEntityCreate<T, T1>
    {
        T createId { get; set; }
        T1 createDt { get; set; }
    }
}
