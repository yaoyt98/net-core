﻿namespace App.SqlSugar
{
    public interface IEntityIsDelete {
        bool isDelete { get; set; }
    }
}
