﻿namespace App.SqlSugar.Interfaces
{
    public interface IEnTityGroupId<T>
    {
        T groupId { get; set; }
    }
}
