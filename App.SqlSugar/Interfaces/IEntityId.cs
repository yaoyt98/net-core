﻿namespace App.SqlSugar {
    public interface IIdEntity<T> {
        T id { get; set; }
    }
}
