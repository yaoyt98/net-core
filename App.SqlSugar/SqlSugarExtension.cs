﻿using Microsoft.Extensions.DependencyInjection;
using SqlSugar;

namespace App.SqlSugar
{
    public static class SqlSugarExtension
    {
        /// <summary>
        /// 注入db
        /// </summary>
        /// <param name="services"></param>
        /// <param name="mySqlDns"></param>
        /// <param name="sqlLog"></param>
        /// <param name="warnSqlLog"></param>
        /// <param name="errorLog"></param>
        public static void UseSqlSugarExtension(this IServiceCollection services, DbType sqlType, string mySqlDns,
            Action<string>? sqlLog, Action<double, string, string>? warnSqlLog,
            Action<string>? errorLog)
        {
            var sqlSugar = SqlSugarHelp.GetSqlSugarConfig(services, sqlType, mySqlDns, sqlLog, warnSqlLog, errorLog);

            services.AddSingleton<ISqlSugarClient>(p => sqlSugar);
        }

        /// <summary>
        /// 注入仓储
        /// </summary>
        /// <param name="services"></param>
        public static void UseSqlSugarRegisterExtension(this IServiceCollection services)
        {
            services.AddScoped(typeof(RegisterBase<>));
        }
    }

    /// <summary>
    /// 自定义仓储
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RegisterBase<T> : SimpleClient<T> where T : class, new()
    {
        public RegisterBase(ISqlSugarClient db)
        {
            base.Context = db;
        }
    }
}
