﻿namespace App.SqlSugar
{
    public class entityUpdate<T, T1> : IEntityUpdate<T, T1>
    {
        public T updateId { get; set; }
        public T1 updateDt { get; set; }
    }
}
