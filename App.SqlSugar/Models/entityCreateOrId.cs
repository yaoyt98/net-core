﻿using System.ComponentModel.DataAnnotations;

namespace App.SqlSugar
{
    public class entityCreateOrId<T, T1,T2> : IIdEntity<T>, IEntityCreate<T1, T2>
    {
        [Key]
        public virtual T id { get; set; }

        public T1 createId { get; set; }
        public T2 createDt { get; set; }
   
    }
}
