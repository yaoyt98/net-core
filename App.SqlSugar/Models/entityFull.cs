﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace App.SqlSugar
{
    /// <summary>
    /// model类
    /// </summary>
    /// <typeparam name="T">id</typeparam>
    /// <typeparam name="T1">创建id,更新id</typeparam>
    /// <typeparam name="T2">创建时间,更新时间</typeparam>
    public class entityFull<T, T1, T2> : IIdEntity<T>, IEntityCreate<T1, T2>, IEntityUpdate<T1, T2>, IEntityIsDelete
    {
        [Key]
        public virtual T id { get; set; }

        [DefaultValue(false)]
        public virtual bool isDelete { get; set; }
        public virtual T1 createId { get; set; }
        public virtual T2 createDt { get; set; }

        public virtual T1? updateId { get; set; }
        public virtual T2? updateDt { get; set; }
    }
}
