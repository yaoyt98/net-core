﻿using System.ComponentModel;

namespace App.SqlSugar
{
    /// <summary>
    /// model类
    /// </summary>
    public class entityDelete : IEntityIsDelete
    {
      
        [DefaultValue(false)]
        public virtual bool isDelete { get; set; }

    }
}
