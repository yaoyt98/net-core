﻿using System.ComponentModel.DataAnnotations;

namespace App.SqlSugar
{
    /// <summary>
    /// 只有id
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class entityId<T> : IIdEntity<T>
    {
        [Key]
        public virtual T id { get; set; }
    }
}
