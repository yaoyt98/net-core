﻿namespace App.SqlSugar
{
    public class entityCreate<T, T1> : IEntityCreate<T, T1>
    {
        public T createId { get; set; }
        public T1 createDt { get; set; }
    }
}
