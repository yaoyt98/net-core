﻿using System.ComponentModel.DataAnnotations;

namespace App.SqlSugar
{
    public class entityUpdate<T, T1, T2> : IIdEntity<T>, IEntityUpdate<T1, T2>
    {
        [Key]
        public virtual T id { get; set; }

        public T1 updateId { get; set; }

        public T2 updateDt { get; set; }
    }
}
