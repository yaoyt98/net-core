﻿using Casbin.AspNetCore.Authorization;
using Serilog;
using System.Security.Claims;

namespace AppTest.Controllers
{
    [Route("")]
    [AppRule("ping", nameof(PingController))]
    [AppActionRule(AppActionRulesHelp.get, nameof(AppActionRulesHelp.get))]
    [AppActionRule(AppActionRulesHelp.add, nameof(AppActionRulesHelp.add))]
    [AppActionRule(AppActionRulesHelp.update, nameof(AppActionRulesHelp.update))]
    public class PingController : AppController
    {
        private readonly ILogger<PingController> _logger;
        public PingController(IMapper mapper, ILogger<PingController> logger) : base(mapper)
        {
            _logger = logger;
        }


        /// <summary>
        /// ping
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("ping")]
        public string ping()
        {
            Log.Information("pong");
            var a = (User.Claims.FirstOrDefault(p => p.Type == "userId")?.Value);
            var claim = User.FindFirst(ClaimTypes.Role);

            return "pong";

        }

        [HttpGet]
        [Route("data1get")]
        [CasbinAuthorize(nameof(PingController), nameof(AppActionRulesHelp.get))]
        public string data1get()
        {
            return "pong";
        }


        [HttpGet]
        [Route("data2add")]
        [CasbinAuthorize(nameof(PingController), nameof(AppActionRulesHelp.add))]
        //RequestTransformerType = typeof(RbacRequestTransformer)
        public string data2add()
        {
            return "pong";
        }

        [HttpGet]
        [Route("data2update")]
        [CasbinAuthorize(nameof(PingController), nameof(AppActionRulesHelp.update))]
        //RequestTransformerType = typeof(RbacRequestTransformer)
        public string data2update()
        {
            return "pong";
        }
    }
}
