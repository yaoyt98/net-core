using App.Channels;
using Serilog;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Add services to the container.
var apiFilename = Assembly.GetExecutingAssembly().GetName().Name;
builder.Services.UseAppExtension(apiFilename);

// 使用jwt
var signKey = builder.Configuration.GetValue<string>("Authen:signKey");
var issuer = builder.Configuration.GetValue<string>("Authen:issuer");
var audience = builder.Configuration.GetValue<string>("Authen:audience");
builder.Services.UseJwtExtension(signKey, issuer, audience);

// 使用内存缓存
builder.Services.UseDefaultCacheExtension();

// 使用日志
var logSqlDns = builder.Configuration.GetConnectionString("logSqlDns");
builder.Services.UseSerilogExtension(logSqlDns, "log");
builder.Host.UseSerilog();
Log.Information("web 日志启动成功");

var dbSqlDns = builder.Configuration.GetConnectionString("dbSqlDns");
// 使用sqlsugar
builder.Services.UseSqlSugarExtension(SqlSugar.DbType.SqlServer, dbSqlDns,
    (sql) =>
    {
        Log.ForContext("level", "info")
        .ForContext("sql", sql)
        .Information("日常:sql执行记录" + sql);
    },
    (time, warnStr, parentWarnStr) =>
    {
        Log.ForContext("level", "warn")
        .ForContext("time", time)
        .ForContext("sql", warnStr)
        .ForContext("parentWarnStr", parentWarnStr)
        .Information("警告:sql执行记录" + warnStr);

    },
    (errorSql) =>
    {
        Log.ForContext("level", "error")
         .ForContext("sql", errorSql)
         .Information("异常:sql执行记录" + errorSql);
    }
);

Log.Information("web 数据库启动成功");

var jobSqlDns = builder.Configuration.GetConnectionString("jobSqlDns");
// 使用hangfire 
builder.Services.UseHangfireExtension(jobSqlDns);

Log.Information("web job启动成功");

// 使用UseCasbin权限服务
builder.Services.UseCasbinExtion("CasbinConfigs", "rbac_model.conf", "rbac_policy.csv", dbSqlDns);
Log.Information("web UseCasbin权限系统启动成功");

builder.Services.AddHostedService<ChannelService>();

var app = builder.Build();

// 使用手动获取di方法
app.UseServiceLocator();
app.UseAppExtension(app.Environment);

// 身份认证
app.UseAuthentication();

//使用Casbin 权限中间件
app.UseCasbinExtion();

// 授权
app.UseAuthorization();

app.MapControllers();

app.UseHangfire();

#if !DEBUG
    AppTest.JobServers.JobManage.RunJob();
              
    Log.Information("Job 启动成功");
#endif

app.Run();
