﻿global using App;
global using App.Serilog;
global using App.AuthenJwt;
global using App.Casbin;
global using App.SqlSugar;
global using App.Job;
global using App.Caches;
global using App.Tool;
global using Hangfire.Common;
global using Hangfire;

global using AutoMapper.Configuration.Annotations;
global using Microsoft.AspNetCore.Authorization;
global using System.ComponentModel.DataAnnotations;
global using AutoMapper;
global using Microsoft.AspNetCore.Mvc;