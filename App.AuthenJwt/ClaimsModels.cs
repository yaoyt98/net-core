﻿namespace App.AuthenJwt
{
    public class ClaimsModels
    {
        public readonly static string UserId = "userId";
        public readonly static string GroupId = "groupId";
        public readonly static string isAdmin = "isAdmin";
        public readonly static string roleIds = "roleIds";
    }
}
