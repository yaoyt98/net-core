﻿using Microsoft.Extensions.DependencyInjection;

namespace App.AuthenJwt
{

    /// <summary>
    /// cors
    /// </summary>
    public static class CorsExtension
    {
        /// <summary>
        /// cors
        /// </summary>
        /// <param name="services"></param>
        /// <param name="ipInput">开启cors ip, 默认所有</param>
        public static void UseCorsExtension(this IServiceCollection services, string ipInput)
        {
            string ip = ipInput;
            if (string.IsNullOrWhiteSpace(ip))
            {
                ip = "*";
            }

            // 启动cors
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(policy =>
                {
                    policy.WithOrigins(ip)
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });
            });
        }
    }
}
